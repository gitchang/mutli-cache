package com.puff.cache.redis;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import com.puff.cache.Cache;
import com.puff.cache.CallBack;

public class RedisCache implements Cache {

	private String cacheName;
	private Redis redis;

	public RedisCache(String cacheName) {
		this.cacheName = cacheName;
		this.redis = Redis.getInstance();
	}

	private String getKeyName(String key) {
		return cacheName + ":" + key;
	}

	@Override
	public boolean exist(String key) {
		return redis.exists(getKeyName(key));
	}

	@Override
	public <T extends Serializable> T get(String key) {
		return redis.get(getKeyName(key));
	}

	@Override
	public <T extends Serializable> T getAndRemove(String key) {
		return redis.getAndRemove(getKeyName(key));
	}

	@Override
	public <T extends Serializable> List<T> get(List<String> keys) {
		if (keys == null || keys.size() == 0) {
			return Collections.emptyList();
		}
		return redis.get(cacheName, keys);
	}

	/**
	 * 有callBack时，忽略从缓存中获取时候的出错
	 */
	@Override
	public <T extends Serializable> T get(String key, CallBack<T> callBack) {
		return redis.get(getKeyName(key), callBack);
	}

	@Override
	public void put(String key, Object value) {
		redis.set(getKeyName(key), value);
	}

	@Override
	public void put(String key, Object value, int expire) {
		redis.setex(getKeyName(key), expire, value);
	}

	@Override
	public void update(String key, Object value) {
		redis.update(getKeyName(key), value);
	}

	@Override
	public int ttl(String key) {
		return redis.ttl(getKeyName(key)).intValue();
	}

	@Override
	public List<String> keys() {
		return redis.keys(cacheName);
	}

	@Override
	public void remove(String key) {
		redis.remove(getKeyName(key));
	}

	@Override
	public void remove(List<String> keys) {
		redis.remove(cacheName, keys);
	}

	@Override
	public void clear() {
		redis.clear(cacheName);
	}

}
