package com.puff.cache.redis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.puff.cache.sync.CacheSyncHandler;
import com.puff.cache.sync.Command;

import redis.clients.jedis.BinaryJedisPubSub;

public class JedisPubSub extends BinaryJedisPubSub {
	private final static Logger log = LoggerFactory.getLogger(JedisPubSub.class);
	private final CacheSyncHandler cacheSync = new CacheSyncHandler();

	/**
	 * 消息接收
	 * 
	 * @param channel 缓存 Channel
	 * @param message 接收到的消息
	 */
	@Override
	public void onMessage(byte[] channel, byte[] message) {
		// 无效消息
		if (message != null && message.length <= 0) {
			log.warn("Message is empty.");
			return;
		}
		try {
			Command cmd = Command.parse(message);
			if (cmd != null && !cmd.msgFromSlef()) {
				cacheSync.handle(cmd);
			}
		} catch (Exception e) {
			log.error("Unable to handle received msg", e);
		}
	}
}
