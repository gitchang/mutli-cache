package com.puff.cache.redis;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.puff.cache.CallBack;
import com.puff.cache.Serialize;

import redis.clients.jedis.BinaryJedisCluster;
import redis.clients.jedis.BinaryJedisPubSub;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.util.SafeEncoder;

/**
 */
public class Redis {

	private final static Logger log = LoggerFactory.getLogger(Redis.class);

	private JedisPool jedisPool;
	private BinaryJedisCluster jedisCluster;
	private boolean cluster = false;

	private static class Inner {
		private static final Redis REDIS = new Redis();
	}

	private Redis() {

	}

	public static Redis getInstance() {
		return Inner.REDIS;
	}

	protected void init(JedisPool jedisPool, BinaryJedisCluster jedisCluster) {
		this.jedisPool = jedisPool;
		this.jedisCluster = jedisCluster;
		cluster = (jedisPool == null && jedisCluster != null);
	}

	public String set(String key, Object value) {
		if (cluster) {
			if (value == null) {
				jedisCluster.del(keyToBytes(key));
				return null;
			} else {
				return jedisCluster.set(keyToBytes(key), valueToBytes(value));
			}
		} else {
			Jedis jedis = getJedis();
			try {
				if (value == null) {
					jedis.del(keyToBytes(key));
					return null;
				} else {
					return jedis.set(keyToBytes(key), valueToBytes(value));
				}
			} finally {
				close(jedis);
			}
		}
	}
	
	
	
	public Long setnx(String key, Object value) {
		if (cluster) {
			if (value == null) {
				jedisCluster.del(keyToBytes(key));
				return null;
			} else {
				return jedisCluster.setnx(keyToBytes(key), valueToBytes(value));
			}
		} else {
			Jedis jedis = getJedis();
			try {
				if (value == null) {
					jedis.del(keyToBytes(key));
					return null;
				} else {
					return jedis.setnx(keyToBytes(key), valueToBytes(value));
				}
			} finally {
				close(jedis);
			}
		}
	}

	public void update(String key, Object value) {
		if (cluster) {
			byte[] bkey = keyToBytes(key);
			if (value == null) {
				jedisCluster.del(bkey);
			} else {
				Long ttl = jedisCluster.ttl(bkey);
				byte[] bvalue = valueToBytes(value);
				if (ttl > 0) {
					jedisCluster.setex(bkey, ttl.intValue(), bvalue);
				} else {
					jedisCluster.set(bkey, bvalue);
				}
			}
		} else {
			Jedis jedis = getJedis();
			try {
				byte[] bkey = keyToBytes(key);
				if (value == null) {
					jedis.del(bkey);
				} else {
					Long ttl = jedis.ttl(bkey);
					byte[] bvalue = valueToBytes(value);
					if (ttl > 0) {
						jedis.setex(bkey, ttl.intValue(), bvalue);
					} else {
						jedis.set(bkey, bvalue);
					}
				}
			} finally {
				close(jedis);
			}
		}
	}

	public String setex(String key, int seconds, Object value) {
		if (cluster) {
			if (value == null) {
				jedisCluster.del(keyToBytes(key));
				return null;
			}
			return jedisCluster.setex(keyToBytes(key), seconds, valueToBytes(value));
		} else {
			Jedis jedis = getJedis();
			try {
				if (value == null) {
					jedis.del(keyToBytes(key));
					return null;
				}
				return jedis.setex(keyToBytes(key), seconds, valueToBytes(value));
			} finally {
				close(jedis);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public <T> T get(String key) {
		if (cluster) {
			return (T) valueFromBytes(jedisCluster.get(keyToBytes(key)));
		} else {
			Jedis jedis = getJedis();
			try {
				return (T) valueFromBytes(jedis.get(keyToBytes(key)));
			} finally {
				close(jedis);
			}
		}
	}

	public <T> T getAndRemove(String key) {
		T result = null;
		byte[] keyBytes = keyToBytes(key);
		if (cluster) {
			result = valueFromBytes(jedisCluster.get(keyBytes));
			if (result != null) {
				jedisCluster.del(keyBytes);
			}
		} else {
			Jedis jedis = getJedis();
			try {
				result = valueFromBytes(jedis.get(keyBytes));
				if (result != null) {
					jedis.del(keyBytes);
				}
			} finally {
				close(jedis);
			}
		}
		return result;
	}

	public <T> List<T> get(String cacheName, List<String> keys) {
		if (cluster) {
			List<T> list = null;
			if (keys != null && keys.size() > 0) {
				list = new ArrayList<T>(keys.size());
				for (String key : keys) {
					byte[] bkey = keyToBytes(cacheName + ":" + key);
					T result = valueFromBytes(jedisCluster.get(bkey));
					list.add(result);
				}
			}
			return list;
		} else {
			Jedis jedis = getJedis();
			try {
				int size = keys.size();
				List<T> list = new ArrayList<T>(size);
				for (int i = 0; i < size; i++) {
					byte[] bkey = keyToBytes(cacheName + ":" + (keys.get(i)));
					T value = valueFromBytes(jedis.get(bkey));
					if (value != null) {
						list.add(value);
					}
				}
				return list;
			} finally {
				close(jedis);
			}
		}
	}

	public <T> T get(String key, CallBack<T> callBack) {
		T result = null;
		byte[] bkey = keyToBytes(key);
		byte[] arr = null;
		boolean cacheError = false;
		if (cluster) {
			try {
				arr = jedisCluster.get(bkey);
			} catch (Exception e) {
				cacheError = true;
				log.error("get from rediscluster error ", e);
			} finally {
				if (arr == null) {
					result = callBack.call(key);
					if (result != null && !cacheError) {
						jedisCluster.set(bkey, valueToBytes(result));
					}
				} else {
					result = valueFromBytes(arr);
				}
			}
		} else {
			Jedis jedis = null;
			try {
				jedis = getJedis();
				arr = jedis.get(bkey);
			} catch (Exception e) {
				cacheError = true;
				log.error("get from redis error ", e);
			} finally {
				try {
					if (arr == null) {
						result = callBack.call(key);
						if (result != null && !cacheError) {
							jedis.set(bkey, valueToBytes(result));
						}
					} else {
						result = valueFromBytes(arr);
					}
				} finally {
					close(jedis);
				}
			}
		}
		return result;
	}

	public void clear(String cacheName) {
		if (cluster) {
			Map<String, JedisPool> nodes = jedisCluster.getClusterNodes();
			if (nodes != null) {
				for (Entry<String, JedisPool> entry : nodes.entrySet()) {
					JedisPool jedisPool = entry.getValue();
					Jedis jedis = jedisPool.getResource();
					try {
						Set<String> keys = jedis.keys(cacheName + ":*");
						if (keys != null) {
							for (String key : keys) {
								jedis.del(keyToBytes(key));
							}
						}
					} finally {
						jedis.close();
					}
				}
			}
		} else {
			Jedis jedis = getJedis();
			try {
				Set<String> keys = jedis.keys(cacheName + ":*");
				if (keys == null || keys.size() == 0) {
					return;
				}
				jedis.del(keysToBytesSet(keys));
			} finally {
				close(jedis);
			}
		}
	}

	public List<String> keys(String cacheName) {
		if (cluster) {
			List<String> result = new ArrayList<String>();
			Map<String, JedisPool> nodes = jedisCluster.getClusterNodes();
			if (nodes != null) {
				Set<String> tmp = new HashSet<String>();
				for (Entry<String, JedisPool> entry : nodes.entrySet()) {
					JedisPool jedisPool = entry.getValue();
					Jedis jedis = jedisPool.getResource();
					try {
						Set<String> keys = jedis.keys(cacheName + ":*");
						if (keys != null) {
							for (String key : keys) {
								tmp.add(key.substring(cacheName.length() + 1));
							}
						}
					} finally {
						jedis.close();
					}
				}
				result.addAll(tmp);
			}
			return result;

		} else {
			Jedis jedis = getJedis();
			try {
				Set<String> keys = jedis.keys(cacheName + "*");
				List<String> result = new ArrayList<String>();
				for (String key : keys) {
					result.add(key.substring(cacheName.length() + 1));
				}
				return result;
			} finally {
				close(jedis);
			}
		}
	}

	public boolean exists(String key) {
		if (cluster) {
			return jedisCluster.exists(keyToBytes(key));
		} else {
			Jedis jedis = getJedis();
			try {
				return jedis.exists(keyToBytes(key));
			} finally {
				close(jedis);
			}
		}
	}

	public Long ttl(String key) {
		if (cluster) {
			return jedisCluster.ttl(keyToBytes(key));
		} else {
			Jedis jedis = getJedis();
			try {
				return jedis.ttl(keyToBytes(key));
			} finally {
				close(jedis);
			}
		}
	}

	public void subscribe(BinaryJedisPubSub jedisPubSub, String... channels) {
		if (channels != null && channels.length > 0) {
			final byte[][] cs = new byte[channels.length][];
			for (int i = 0; i < cs.length; i++) {
				cs[i] = SafeEncoder.encode(channels[i]);
			}
			if (!cluster) {
				jedisCluster.subscribe(jedisPubSub, cs);
			} else {
				Jedis jedis = getJedis();
				try {
					jedis.subscribe(jedisPubSub, cs);
				} finally {
					close(jedis);
				}
			}
		}
	}

	public void publish(String channel, byte[] message) {
		if (cluster) {
			jedisCluster.publish(keyToBytes(channel), message);
		} else {
			Jedis jedis = getJedis();
			try {
				jedis.publish(keyToBytes(channel), message);
			} finally {
				close(jedis);
			}
		}
	}

	// ---------

	private byte[] keyToBytes(String key) {
		return SafeEncoder.encode(key);
	}

	private byte[] keyToBytes(Object key) {
		return SafeEncoder.encode(key.toString());
	}

	private <T> byte[][] keysToBytesSet(Set<T> keys) {
		if (keys == null) {
			return null;
		}
		byte[][] result = new byte[keys.size()][];
		int i = 0;
		for (T key : keys) {
			result[i++] = keyToBytes(key);
		}
		return result;
	}

	private byte[] valueToBytes(Object object) {
		return Serialize.fstserialize(object);
	}

	private <T> T valueFromBytes(byte[] bytes) {
		if (bytes != null)
			return Serialize.fstdeserialize(bytes);
		return null;
	}

	public Jedis getJedis() {
		return jedisPool.getResource();
	}

	public void close(Jedis jedis) {
		jedis.close();
	}

	public void remove(String key) {
		if (cluster) {
			jedisCluster.del(keyToBytes(key));
		} else {
			Jedis jedis = getJedis();
			try {
				jedis.del(keyToBytes(key));
			} finally {
				close(jedis);
			}
		}
	}

	public void remove(String cacheName, List<String> keys) {
		if (keys == null || keys.size() == 0) {
			return;
		}
		if (cluster) {
			for (String key : keys) {
				jedisCluster.del(keyToBytes(cacheName + ":" + key));
			}
		} else {
			int size = keys.size();
			byte[][] result = new byte[size][];
			for (int i = 0; i < result.length; i++)
				result[i] = keyToBytes(cacheName + ":" + keys.get(i));
			Jedis jedis = getJedis();
			try {
				jedis.del(result);
			} finally {
				close(jedis);
			}
		}

	}

	public void close() {
		if (cluster) {
			try {
				jedisCluster.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			jedisPool.close();
		}
	}

	public JedisPool getJedisPool() {
		return jedisPool;
	}

	public BinaryJedisCluster getJedisCluster() {
		return jedisCluster;
	}

	public boolean isCluster() {
		return cluster;
	}

	public void expire(String string, int i) {
		
		if (cluster) {
			jedisCluster.expire(keyToBytes(string), i);
		} else {
			Jedis jedis = getJedis();
			try {
				jedis.expire(keyToBytes(string), i);
			} finally {
				close(jedis);
			}
		}
	}


}
