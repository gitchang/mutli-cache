package com.puff.cache.redis;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import com.puff.cache.Cache;
import com.puff.cache.CacheManager;

import redis.clients.jedis.BinaryJedisCluster;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class RedisCacheManager implements CacheManager {

	public String cacheType() {
		return "Redis";
	}

	public void start(Properties props) {
		props = getProviderProperties(props);
		Integer timeout = getProperty(props, "timeout", 2000);
		String password = getProperty(props, "password", null);
		Integer database = getProperty(props, "database", 1);
		String clientName = getProperty(props, "clientName", null);
		JedisPoolConfig jpc = new JedisPoolConfig();
		boolean testWhileIdle = getProperty(props, "testWhileIdle", false);
		jpc.setTestWhileIdle(testWhileIdle);
		long minEvictableIdleTimeMillis = getProperty(props, "minEvictableIdleTimeMillis", 1000);
		jpc.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
		long timeBetweenEvictionRunsMillis = getProperty(props, "timeBetweenEvictionRunsMillis", 10);
		jpc.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
		int numTestsPerEvictionRun = getProperty(props, "numTestsPerEvictionRun", 10);
		jpc.setNumTestsPerEvictionRun(numTestsPerEvictionRun);

		jpc.setLifo(getProperty(props, "lifo", false));
		jpc.setMaxTotal(getProperty(props, "maxTotal", 500));
		jpc.setMinIdle(getProperty(props, "minIdle", 20));
		jpc.setMaxIdle(getProperty(props, "maxIdle", 200));
		jpc.setMaxWaitMillis(getProperty(props, "maxWaitMillis", 3000));

		jpc.setTestWhileIdle(getProperty(props, "testWhileIdle", false));
		jpc.setTestOnBorrow(getProperty(props, "testOnBorrow", true));
		jpc.setTestOnReturn(getProperty(props, "testOnReturn", false));
		jpc.setTestOnCreate(getProperty(props, "testOnCreate", false));
		Integer maxRedirections = getProperty(props, "maxRedirections", 20);

		JedisPool jedisPool = null;
		BinaryJedisCluster jedisCluster = null;

		String address = getProperty(props, "address", "127.0.0.1:6379");
		String[] arr = address.split(",");
		if (arr != null && arr.length > 1) {
			Set<HostAndPort> set = new HashSet<HostAndPort>();
			for (String server : arr) {
				String[] tmp = server.split(":");
				if (tmp.length >= 2) {
					set.add(new HostAndPort(tmp[0], Integer.parseInt(tmp[1])));
				}
			}
			jedisCluster = new BinaryJedisCluster(set, timeout, maxRedirections, jpc);
		} else {
			String[] server = arr[0].split(":");
			String host = server[0];
			Integer port = Integer.parseInt(server[1]);
			if (port != null && timeout != null && password != null && database != null && database != -1 && clientName != null)
				jedisPool = new JedisPool(jpc, host, port, timeout, password, database, clientName);
			else if (port != null && timeout != null && password != null && database != null && database != -1)
				jedisPool = new JedisPool(jpc, host, port, timeout, password, database);
			else if (port != null && timeout != null && password != null)
				jedisPool = new JedisPool(jpc, host, port, timeout, password);
			else if (port != null && timeout != null)
				jedisPool = new JedisPool(jpc, host, port, timeout);
			else
				jedisPool = new JedisPool(jpc, host, port);
		}
		Redis.getInstance().init(jedisPool, jedisCluster);
	}

	public void stop() {
		Redis.getInstance().close();
	}

	public Cache buildCache(String cacheName) {
		return new RedisCache(cacheName);
	}

	public void destroy(String cacheName) {

	}

	private final Properties getProviderProperties(Properties props) {
		Properties new_props = new Properties();
		Enumeration<Object> keys = props.keys();
		String prefix = "redis.";
		while (keys.hasMoreElements()) {
			String key = (String) keys.nextElement();
			if (key.startsWith(prefix)) {
				new_props.setProperty(key.substring(prefix.length()), props.getProperty(key));
			}
		}
		return new_props;
	}

	private String getProperty(Properties props, String key, String defaultValue) {
		String value = props.getProperty(key, defaultValue);
		return value != null ? value.trim() : null;
	}

	private int getProperty(Properties props, String key, int defaultValue) {
		try {
			return Integer.parseInt(props.getProperty(key, String.valueOf(defaultValue)).trim());
		} catch (Exception e) {
			return defaultValue;
		}
	}

	private boolean getProperty(Properties props, String key, boolean defaultValue) {
		return "true".equalsIgnoreCase(props.getProperty(key, String.valueOf(defaultValue)).trim());
	}

}
