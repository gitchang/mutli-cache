package com.puff.cache.sync;

/**
 * 缓存同步
 * @author dongchao
 *
 */
public interface CacheSync {

	/**
	 * 初始化
	 */
	public CacheSync init();

	/**
	 * 发送命令
	 * @param command
	 */
	public void sendCommand(Command command);

}
