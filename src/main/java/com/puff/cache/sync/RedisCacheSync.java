package com.puff.cache.sync;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.puff.cache.redis.JedisPubSub;
import com.puff.cache.redis.Redis;

/**
 * Redis缓存同步
 * 
 */
public class RedisCacheSync implements CacheSync {
	private final static Logger log = LoggerFactory.getLogger(RedisCacheSync.class);
	private final Redis redis = Redis.getInstance();
	private final static String channel = "MutliCache-Redis-PubSub";

	@Override
	public CacheSync init() {
		long ct = System.currentTimeMillis();
		Thread thread_subscribe = new Thread(new Runnable() {
			private final JedisPubSub jedisPubSub = new JedisPubSub();

			@Override
			public void run() {
				try {
					redis.subscribe(jedisPubSub, channel);
				} catch (Exception e) {
				}
			}
		});
		thread_subscribe.setName(channel);
		thread_subscribe.setDaemon(true);
		thread_subscribe.setPriority(5);
		thread_subscribe.start();
		log.info("Connected to RedisPubSub:" + channel + ", time " + (System.currentTimeMillis() - ct) + " ms.");
		return this;
	}

	@Override
	public void sendCommand(Command command) {
		redis.publish(channel, command.toBuffers());
	}

}
