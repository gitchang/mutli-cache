package com.puff.cache;

import java.util.Properties;

import com.puff.cache.ehcache.EHCacheManager;
import com.puff.cache.multilevel.MultiLevelCacheManager;
import com.puff.cache.redis.RedisCacheManager;

public class MultiLevelCacheTest {

	public static void main(String[] args) {
		EHCacheManager ehCacheManager = new EHCacheManager();

		RedisCacheManager redisCacheManager = new RedisCacheManager();

		MultiLevelCacheManager manager = new MultiLevelCacheManager(ehCacheManager, redisCacheManager);
		manager.start(new Properties());
		Cache cache = manager.buildCache("person");

		String name = cache.get("name");
		System.out.println(name);
		
		cache.put("name", "dongchao");
		name = cache.get("name");
		System.out.println(name);

	}

}
